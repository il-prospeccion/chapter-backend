import { Router } from "express";
const router = Router();

import { getServicio } from "../controllers/miservicio.controller";

router.route('/')
    .get(getServicio);
export default router;