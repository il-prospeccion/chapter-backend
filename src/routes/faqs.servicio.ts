import { Router } from "express";
const router = Router();

import { getFaqs } from "../controllers/faqs.controller";

router.route('/')
    .get(getFaqs);
export default router;