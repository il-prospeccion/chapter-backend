import { Router } from "express";
const router = Router();

import { getCanales } from "../controllers/canales.controller";

router.route('/')
    .get(getCanales);
export default router;