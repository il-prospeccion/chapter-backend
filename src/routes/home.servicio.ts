import { Router } from "express";
const router = Router();

import { getHome } from "../controllers/home.controller";

router.route('/')
    //.get((req, res) => res.json('HOME'))
    .get(getHome);
export default router;