import { Request, Response } from "express";
import { connect } from "../database";

export async function getHome(req: Request, res: Response): Promise<Response> {
    const conn = await connect();
    const home = await conn.query('SELECT * FROM ACERCADETI WHERE ACTIVO=1');
    return res.json(home[0]);
}