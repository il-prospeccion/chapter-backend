import { Console } from "console";
import { json, Request, Response } from "express";
import { connect } from "../database";

export interface Service {
  IDSERVICIO?: Number,
  NOMBRE?: string,
  DESCRIPCION?: string,
  SUPERVISOR?: string
}
export interface Category {
  ID_CATEGORIA?: number,
  DESCRIPCION?: String
}
export interface ServicesByCategory {
  CATEGORIA?: Category,
  SERVICIOS?: Array<Service>
}

export async function getServicio(req: Request, res: Response): Promise<Response> {
  const conn = await connect();

  const categorias = await conn.query('call sp_get_categories()')
  let jsonCategories = JSON.parse(JSON.stringify(categorias[0]))[0];

  let kv1: Category;
  let kv2: Array<Service>;
  let kv3: ServicesByCategory;
  let kv4: Array<ServicesByCategory> = [];

  for (var k in jsonCategories) {
    kv1 = jsonCategories[k]
    console.log(kv1.ID_CATEGORIA)
    const servicios = await conn.query(`CALL sp_get_services_by_category(${kv1.ID_CATEGORIA})`);
    let jsonServicesByCategory = JSON.parse(JSON.stringify(servicios[0]))[0];
    kv2 = jsonServicesByCategory;
    kv3 = { CATEGORIA: kv1, SERVICIOS: kv2 }
    kv4.push(kv3)
  }
  return res.json(kv4);
}