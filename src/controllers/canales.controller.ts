import { Request, Response } from "express";
import { connect } from "../database";

export async function getCanales(req: Request, res: Response): Promise<Response> {
    const conn = await connect();
    const canales = await conn.query('SELECT * FROM CANAL WHERE ESTADO=1');
    return res.json(canales[0]);
}