import { Request, Response } from "express";
import { connect } from "../database";

export async function getFaqs(req: Request, res: Response): Promise<Response> {
    const conn = await connect();
    const faqs = await conn.query('SELECT F.ID_FAQ, F.PREGUNTA, F.RESPUESTA, C.DESCRIPCION CATEGORIA, S.NOMBRE SERVICIO FROM FAQS F LEFT JOIN CATEGORIA C ON C.ID_CATEGORIA=F.ID_CATEGORIA LEFT JOIN SERVICIO S ON S.IDSERVICIO=F.ID_SERVICIO WHERE F.ESTADO=1 AND S.ESTADO=1 AND C.ESTADO=1;');
    return res.json(faqs[0]);
}