import { App } from './app'

async function main(){
    const app = new App(3030);
    await app.listen();
}

main();