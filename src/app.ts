import express, { Application } from "express";
import morgan from "morgan";

//Routes
import IndexRoutes from './routes/index.servicio'
import HomeRoutes from './routes/home.servicio'
import ServicioRoutes from './routes/mi.servicio'
import CanalesRoutes from './routes/canales.servicio'
import FaqsRoutes from './routes/faqs.servicio'


export class App {

    private app: Application;

    constructor(private port?: number | string) {
        this.app = express();
        this.settings();
        this.middlewares();
        this.routes();
    }

    settings (){
        this.app.set('port',this.port || process.env.PORT || 9999)
    }

    middlewares(){
        //this.app.use(morgan('dev'));
    } 

    routes(){
        this.app.use(IndexRoutes);
        this.app.use('/acercadeti', HomeRoutes);
        this.app.use('/servicios', ServicioRoutes);
        this.app.use('/canales', CanalesRoutes);    
        this.app.use('/faqs', FaqsRoutes);
    }

    async listen() {
        await this.app.listen(this.app.get('port'));
        console.log ("Server on port",3030);
    }

}

