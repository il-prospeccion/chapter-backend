import { createPool } from 'mysql2/promise';

export async function connect(){
    
    const connection = await createPool( {     
        /*host:'bdutpdev.c30arlrgejdz.us-east-1.rds.amazonaws.com',
        user:'usr_chapter',
        password:'*E8sMrtu9Kjb',
        database: 'db_servicios',
        connectionLimit: 10*/     
        
        host:'localhost',
        user:'root',
        password:'root',
        database: 'db_servicios',
        connectionLimit: 10
       
    });
    return connection;
}